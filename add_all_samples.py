#!/usr/bin/env python

# Standard packages
import os
import re
import sys
import csv
import cyvcf2
import getpass
import argparse

import utils
import vcf_parsing
import configuration

from cyvcf2 import VCF
from datetime import datetime
from multiprocessing import Pool
from collections import defaultdict

from cassandra import WriteFailure
from cassandra import InvalidRequest
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from variantstore import Variant
from variantstore import SampleVariant
from variantstore import TargetVariant

def add_sample(row_data):
    connection.setup([row_data[-2]], "variantstore", auth_provider=row_data[-1])

    config = row_data[-3]
    samples_config_file = row_data[2]
    sample = row_data[0]
    library = row_data[1]
    libraries = configuration.configure_samples(samples_config_file, config)
    samples = configuration.merge_library_configs_samples(libraries)

    parse_functions = {'mutect': vcf_parsing.parse_mutect_vcf_record,
                       'freebayes': vcf_parsing.parse_freebayes_vcf_record,
                       'vardict': vcf_parsing.parse_vardict_vcf_record,
                       'scalpel': vcf_parsing.parse_scalpel_vcf_record,
                       'platypus': vcf_parsing.parse_platypus_vcf_record,
                       'pindel': vcf_parsing.parse_pindel_vcf_record}

    caller_records = defaultdict(lambda: dict())

    file_problem = False
    if not os.path.exists(row_data[4]):
        file_problem = True
    if not os.path.exists(row_data[5]):
        file_problem = True
    if not os.path.exists(row_data[6]):
        file_problem = True
    if not os.path.exists(row_data[7]):
        file_problem = True
    if not os.path.exists(row_data[8]):
        file_problem = True
    if not os.path.exists(row_data[9]):
        file_problem = True
    if not os.path.exists(row_data[10]):
        file_problem = True

    if file_problem:
        with open(f"{sample}.{library}.ERRORS.txt", 'w') as error:
            error.write(f"Could not open one or more files for sample: {sample} and library: {library}\n")
        return

    vcf_parsing.parse_vcf(row_data[5], "mutect", caller_records)
    vcf_parsing.parse_vcf(row_data[6], "vardict", caller_records)
    vcf_parsing.parse_vcf(row_data[7], "freebayes", caller_records)
    vcf_parsing.parse_vcf(row_data[8], "scalpel", caller_records)
    vcf_parsing.parse_vcf(row_data[9], "platypus", caller_records)
    vcf_parsing.parse_vcf(row_data[10], "pindel", caller_records)

    annotated_vcf = row_data[4]

    vcf = VCF(annotated_vcf)

    reader = cyvcf2.VCFReader(annotated_vcf)
    desc = reader["ANN"]["Description"]
    annotation_keys = [x.strip("\"'") for x in re.split("\s*\|\s*", desc.split(":", 1)[1].strip('" '))]

    # Filter out variants with minor allele frequencies above the threshold but
    # retain any that are above the threshold but in COSMIC or in ClinVar and
    # not listed as benign.
    added = 0
    failed = 0
    for variant in vcf:
        # Parsing VCF and creating data structures for Cassandra model
        callers = variant.INFO.get('CALLERS').split(',')
        effects = utils.get_effects(variant, annotation_keys)
        top_impact = utils.get_top_impact(effects)
        population_freqs = utils.get_population_freqs(variant)
        amplicon_data = utils.get_amplicon_data(variant)

        key = (str("chr{}".format(variant.CHROM)), int(variant.start), int(variant.end), str(variant.REF),
               str(variant.ALT[0]))

        caller_variant_data_dicts = defaultdict(dict)
        max_som_aaf = -1.00
        max_depth = -1
        min_depth = 100000000

        fail_filter = False

        for caller in callers:
            caller_variant_data_dicts[caller] = parse_functions[caller](caller_records[caller][key])
            if float(caller_variant_data_dicts[caller]['AAF']) > max_som_aaf:
                max_som_aaf = float(caller_variant_data_dicts[caller]['AAF'])
            if int(caller_variant_data_dicts[caller]['DP']) < min_depth:
                min_depth = int(caller_variant_data_dicts[caller]['DP'])
            if int(caller_variant_data_dicts[caller]['DP']) > max_depth:
                max_depth = int(caller_variant_data_dicts[caller]['DP'])

        if min_depth == 100000000:
            min_depth = -1

        if max_depth < 100:
            fail_filter = True
        if max_som_aaf < 0.01:
            fail_filter = True
        if len(callers) == 1:
            if callers[0] == "freebayes" or callers[0] == "pindel":
                fail_filter = True

        if fail_filter:
            continue

        # Create Cassandra Objects
        # Create the general variant ordered table
        try:
            cassandra_variant = Variant.create(
                    reference_genome="GRCh37.75",
                    chr=variant.CHROM,
                    pos=variant.start,
                    end=variant.end,
                    ref=variant.REF,
                    alt=variant.ALT[0],
                    sample=samples[sample][library]['sample_name'],
                    extraction=samples[sample][library]['extraction'],
                    library_name=samples[sample][library]['library_name'],
                    run_id=samples[sample][library]['run_id'],
                    panel_name=samples[sample][library]['panel'],
                    # initial_report_panel=samples[sample][library]['report'],
                    target_pool=samples[sample][library]['target_pool'],
                    sequencer=samples[sample][library]['sequencer'],
                    rs_id=variant.ID,
                    date_annotated=datetime.today(),
                    subtype=variant.INFO.get('sub_type'),
                    type=variant.INFO.get('type'),

                    gene=top_impact.gene,
                    transcript=top_impact.transcript,
                    exon=top_impact.exon,
                    codon_change=top_impact.codon_change,
                    biotype=top_impact.biotype,
                    aa_change=top_impact.aa_change,
                    severity=top_impact.effect_severity,
                    impact=top_impact.top_consequence,
                    impact_so=top_impact.so,

                    max_maf_all=variant.INFO.get('max_aaf_all') or -1,
                    max_maf_no_fin=variant.INFO.get('max_aaf_no_fin') or -1,
                    transcripts_data=utils.get_transcript_effects(effects),
                    clinvar_data=utils.get_clinvar_info(variant, samples, sample),
                    cosmic_data=utils.get_cosmic_info(variant),
                    in_clinvar=vcf_parsing.var_is_in_clinvar(variant),
                    in_cosmic=vcf_parsing.var_is_in_cosmic(variant),
                    is_pathogenic=vcf_parsing.var_is_pathogenic(variant),
                    is_lof=vcf_parsing.var_is_lof(variant),
                    is_coding=vcf_parsing.var_is_coding(variant),
                    is_splicing=vcf_parsing.var_is_splicing(variant),
                    rs_ids=vcf_parsing.parse_rs_ids(variant),
                    cosmic_ids=vcf_parsing.parse_cosmic_ids(variant),
                    callers=callers,
                    population_freqs=population_freqs,
                    amplicon_data=amplicon_data,
                    max_som_aaf=max_som_aaf,
                    min_depth=min_depth,
                    max_depth=max_depth,
                    mutect=caller_variant_data_dicts['mutect'] or dict(),
                    freebayes=caller_variant_data_dicts['freebayes'] or dict(),
                    scalpel=caller_variant_data_dicts['scalpel'] or dict(),
                    platypus=caller_variant_data_dicts['platypus'] or dict(),
                    pindel=caller_variant_data_dicts['pindel'] or dict(),
                    vardict=caller_variant_data_dicts['vardict'] or dict()
                    )
        except KeyError:
            sys.stderr.write(f"KeyError: {sample}\n")
        except WriteFailure:
            with open("{}.sample_variant_add.log".format(samples[sample][library]['library_name']), "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write("Sample: {}\t Library: {}\n".format(samples[sample][library]['sample_name'],
                                                              samples[sample][library]['library_name'],))
                err.write("{}\n".format(variant))
        except InvalidRequest:
            with open("{}.sample_variant_add.log".format(samples[sample][library]['library_name']), "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write("Sample: {}\t Library: {}\n".format(samples[sample][library]['sample_name'],
                                                              samples[sample][library]['library_name'], ))
                err.write("{}\n".format(variant))

        # Create Cassandra Object
        try:
            sample_variant = SampleVariant.create(
                    sample=samples[sample][library]['sample_name'],
                    run_id=samples[sample][library]['run_id'],
                    library_name=samples[sample][library]['library_name'],
                    reference_genome="GRCh37.75",
                    chr=variant.CHROM,
                    pos=variant.start,
                    end=variant.end,
                    ref=variant.REF,
                    alt=variant.ALT[0],
                    extraction=samples[sample][library]['extraction'],
                    panel_name=samples[sample][library]['panel'],
                    # initial_report_panel=samples[sample][library]['report'],
                    target_pool=samples[sample][library]['target_pool'],
                    sequencer=samples[sample][library]['sequencer'],
                    rs_id=variant.ID,
                    date_annotated=datetime.today(),
                    subtype=variant.INFO.get('sub_type'),
                    type=variant.INFO.get('type'),
                    gene=top_impact.gene,
                    transcript=top_impact.transcript,
                    exon=top_impact.exon,
                    codon_change=top_impact.codon_change,
                    biotype=top_impact.biotype,
                    aa_change=top_impact.aa_change,
                    severity=top_impact.effect_severity,
                    impact=top_impact.top_consequence,
                    impact_so=top_impact.so,
                    max_maf_all=variant.INFO.get('max_aaf_all') or -1,
                    max_maf_no_fin=variant.INFO.get('max_aaf_no_fin') or -1,
                    transcripts_data=utils.get_transcript_effects(effects),
                    clinvar_data=utils.get_clinvar_info(variant, samples, sample),
                    cosmic_data=utils.get_cosmic_info(variant),
                    in_clinvar=vcf_parsing.var_is_in_clinvar(variant),
                    in_cosmic=vcf_parsing.var_is_in_cosmic(variant),
                    is_pathogenic=vcf_parsing.var_is_pathogenic(variant),
                    is_lof=vcf_parsing.var_is_lof(variant),
                    is_coding=vcf_parsing.var_is_coding(variant),
                    is_splicing=vcf_parsing.var_is_splicing(variant),
                    rs_ids=vcf_parsing.parse_rs_ids(variant),
                    cosmic_ids=vcf_parsing.parse_cosmic_ids(variant),
                    callers=callers,
                    population_freqs=population_freqs,
                    amplicon_data=amplicon_data,
                    max_som_aaf=max_som_aaf,
                    min_depth=min_depth,
                    max_depth=max_depth,
                    mutect=caller_variant_data_dicts['mutect'] or dict(),
                    freebayes=caller_variant_data_dicts['freebayes'] or dict(),
                    scalpel=caller_variant_data_dicts['scalpel'] or dict(),
                    platypus=caller_variant_data_dicts['platypus'] or dict(),
                    pindel=caller_variant_data_dicts['pindel'] or dict(),
                    vardict=caller_variant_data_dicts['vardict'] or dict(),
                    manta=caller_variant_data_dicts['manta'] or dict()
                    )
        except WriteFailure:
            failed += 1
            with open("{}.sample_variant_add.log".format(samples[sample][library]['library_name']), "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write("Sample: {}\t Library: {}\n".format(samples[sample][library]['sample_name'],
                                                              samples[sample][library]['library_name'],))
                err.write("{}\n".format(variant))
        except InvalidRequest:
            with open("{}.sample_variant_add.log".format(samples[sample][library]['library_name']), "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write("Sample: {}\t Library: {}\n".format(samples[sample][library]['sample_name'],
                                                              samples[sample][library]['library_name'], ))
        added += 1

    with open("{}.sample_variant_add.log".format(samples[sample][library]['library_name']), "a") as err:
        err.write("Sample: {}\t Library: {}\n".format(samples[sample][library]['sample_name'],
                                                      samples[sample][library]['library_name'], ))
        err.write("Wrote {} variants to variantstore\n".format(added))
        err.write("Failed to add {} variants to variantstore\n".format(failed))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection", default='127.0.0.1')
    parser.add_argument('-u', '--username', help='Cassandra username for login', default=None)
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    args = parser.parse_args()
    args.logLevel = "INFO"

    num_cores = 24

    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
    else:
        auth_provider = None

    pool = Pool(num_cores)
    file_data = list()
    config = configuration.configure_runtime(args.configuration)

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            row.extend([config, args.address, auth_provider])
            file_data.append(row)

    results = pool.map(add_sample, file_data)
